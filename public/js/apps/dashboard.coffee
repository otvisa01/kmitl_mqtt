define (require) ->
  $ = require('jquery')
  mqtt = require('browserMqtt')
  client = null

  $('#connect_btn').click ()->
    hostname = $('#hostname').val();
    port = $('#port').val();

    opts = {};
    client_id = $('#client_id').val();
    if client_id
      opts.clientId = client_id
    username = $('#username').val();
    if username
      opts.username = username
    password = $('#password').val();
    if password
      opts.password = password

    console.log("opts", opts);

    client = mqtt.connect("mqtt://"+hostname+":"+port, opts);
    client.on "connect", () ->
      console.log("Connect Success, CLIENT ID : ", client.options.clientId);
      $('#messages').append("<li>Connected, client id " + client.options.clientId + "</li>")
      return
    client.on "error", (err) ->
      alert(err)
      client.end()
      return

    client.on "message", (topic, message, packet) ->
      console.log(topic, message, packet)
      $('#messages').append("<li>" + client.options.clientId + "["  + topic + "] : " + message + "</li>")
      return
    return

  $('#publish_btn').click ()->
    if client.connected
      topic = $('#topic').val();
      message = $('#message').val();
      client.publish(topic, message);
      $('#message').val("");
      return
    return

  $('#subscribe_btn').click ()->
    if client.connected
      topic = $('#subscribe_topic').val();
      client.subscribe(topic, (err, granted) ->
        console.log(err, granted)
        $('#messages').append("<li>" + client.options.clientId + "["  + topic + "] : Subscribe</li>")
      )
      return
    return
  return