requirejs.config({
  baseUrl: 'js/libs',
  paths: {
    bootstrap: 'bootstrap.min',
    jquery: 'jquery-1.11.2.min',
    apps: '../apps'
  }
});