// Simple route middleware to ensure user is authenticated.
middlewares = {};
middlewares.ensureAuthenticated = function(req, res, next) {
    if (req.isAuthenticated()) { return next(); }
    req.flash('error', 'Please sign in!');
    res.redirect('/');
};
module.exports = middlewares;