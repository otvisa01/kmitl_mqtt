var express = require('express')
    , passport = require('passport')
    , User = require('../models/user')
    , router = express.Router()
    , LocalStrategy = require('passport-local').Strategy;

passport.serializeUser(function(user, done) {
    done(null, user.id);
});

passport.deserializeUser(function(id, done) {
    User.findById(id, function (err, user) {
        done(err, user);
    });
});

passport.use(new LocalStrategy(function(username, password, done) {
    User.findOne({ username: username }, function(err, user) {
        if (err) { return done(err); }
        if (!user) { return done(null, false, { message: 'Unknown user ' + username }); }
        user.comparePassword(password, function(err, isMatch) {
            if (err) return done(err);
            if(isMatch) {
                return done(null, user);
            } else {
                return done(null, false, { message: 'Invalid password' });
            }
        });
    });
}));

router.post('/login', function(req, res, next) {
    passport.authenticate('local', function(err, user, info) {
        if (err || !user) {
            req.flash('error', info.message);
            return res.redirect('/')
        }
        req.logIn(user, function(err) {
            if (err) {
                console.log(err);
                return next(err);
            }
            req.flash('success', "Login success");
            return res.redirect('/dashboard');
        });
    })(req, res, next);
});

router.get('/logout', function(req, res){
    req.logout();
    res.redirect('/');
});

router.get('/register', function(req, res, next) {
    var error_messages = req.flash('error');
    res.render('accounts/register', { user: req.user, error_messages: error_messages});
});
router.post('/register', function(req, res, next) {
    var name = req.body.name;
    var student_id = req.body.student_id;
    var username = req.body.username;
    var password = req.body.password;
    if(password && student_id && username && name){
        User.findOne({ username: username }, function(err, user) {
            if (user) {
                req.flash('error', 'Already have username "' + username + '" please use another username.');
                res.redirect('/register');
            }else{
                var usr = new User();
                usr.name = name;
                usr.student_id = student_id;
                usr.username = username;
                usr.password = password;
                usr.save(function(error){
                    if(!error){
                        req.flash('success', 'Register success.');
                        res.redirect('/');
                    }
                });
            }
        });
    }else{
        req.flash('error', 'Please fill all field');
        res.redirect('/');
    }
});

module.exports = router;