var express = require('express')
    , passport = require('passport')
    , router = express.Router();

router.use('', require('./accounts'));
router.use('', require('./dashboard'));
router.get('/', function(req, res) {
    var success_messages = req.flash('success');
    var error_messages = req.flash('error');
    res.render('index', { user: req.user, error_messages: error_messages, success_messages: success_messages});
});
router.get('/logs', function(req, res) {
    res.render('logs');
});

module.exports = router;