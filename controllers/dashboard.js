var express = require('express')
    , passport = require('passport')
    , auth_middleware = require('../middlewares/auth')
    , router = express.Router();

router.get('/dashboard', auth_middleware.ensureAuthenticated, function(req, res, next) {
    var error_messages = req.flash('error');
    res.render('dashboard', { user: req.user, error_messages: error_messages});
});
module.exports = router;