var mosca = require('mosca')
    , express = require('express')
    , session = require('express-session')
    , bodyParser = require('body-parser')
    , cookieParser = require('cookie-parser')
    , flash = require('connect-flash')
    , passport = require('passport')
    , path = require('path')
    , http = require("http")
    , User = require("./models/user")
    , faye = require('faye')
    , app = express();

var mosca_server = new mosca.Server({
    port: 1883,
    backend: {
	type: 'redis',
  	redis: require('redis'),
  	db: 12,
  	port: 6379,
  	return_buffers: true, // to handle binary payloads
  	host: "localhost"
    }
});

var mosa_log = function(msg){
    console.log(msg);
    /*mosca_server.publish({
        topic: 'logs',
        payload: msg,
        qos: 0, // 0, 1, or 2
        retain: false // or true
    }, function(err){});*/
};
mosca_server.on('clientConnected', function(client) {
    mosa_log('Client [' + client.id + '] Connected');
});
mosca_server.on('clientDisconnected', function(client) {
    mosa_log('Client [' + client.id + '] Disconnected');
});

mosca_server.on('subscribed', function(topic, client) {
    if(topic != "logs" && topic.split("/")[0] != "$SYS"){
        mosa_log('Client [' +client.id+ '] Subscribed to ' + topic);
    }
});

// fired when a message is received
mosca_server.on('published', function(packet, client) {
    if(packet.topic && 
packet.topic != 'logs' && packet.topic.split("/")[0] != "$SYS"){
        mosa_log('Published msg [' + packet.payload + '] to [' + packet.topic + ']');
    }
});

mosca_server.on('ready', setup);
mosca_server.attachHttpServer(app);

// fired when the mqtt server is ready
function setup() {
    console.log('Mosca server is up and running');
    mosca_server.authenticate = function(client, username, password, callback) {
        callback(null, true);
        //if(username == "logs" && password.toString() == "saran1@#$"){
        //    callback(null, true);
        //}else{
        //    if(password)
        //        password = password.toString();
        //    console.log("AUTH", username, password);
        //    User.findOne({ username: username }, function(err, user) {
        //        if (user) {
        //            user.comparePassword(password, function(err, isMatch) {
        //                if(isMatch) {
        //                    client.user = username;
        //                    callback(null, true);
        //                } else {
        //                    callback(null, false);
        //                }
        //            });
        //        }else{
        //            callback(null, false);
        //        }
        //    });
        //}
    };
    mosca_server.authorizePublish = function(client, topic, payload, callback) {
        //client.user == topic.split('/')[1]
        callback(null, true);
    };
    mosca_server.authorizeSubscribe = function(client, topic, callback) {
        callback(null, true);
    };
}

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(cookieParser('tset2348s7d9ah^%&SDhjbasd'));
app.use(session({cookie: { maxAge: 60000 }}));
app.use(flash());

app.use(passport.initialize());
app.use(passport.session());

app.use(express.static(__dirname + '/public'));
app.use(require('./controllers'));

app.engine('jade', require('jade').__express);
app.set('view engine', 'jade');

var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/test');

app.server = http.createServer(app);
mosca_server.attachHttpServer(app.server);
app.server.listen(3000, function() {
    console.log('Listening on port 3000...')
});
